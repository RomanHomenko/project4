//
//  ViewController.swift
//  Project4
//
//  Created by Роман Хоменко on 29.03.2022.
//

import UIKit

class ViewController: UITableViewController {
    var webSites = ["apple.com", "hackingwithswift.com", "vk.com"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Small browser"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.view.backgroundColor = .white
        self.edgesForExtendedLayout = UIRectEdge.bottom
    }
}

extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return webSites.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Site", for: indexPath)
        cell.textLabel?.text = webSites[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailVC = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
            
            if webSites[indexPath.row] == "vk.com" {
                let alert = UIAlertController(title: "You dont allow to visit this site", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                present(alert, animated: true)
                return
            }
            detailVC.webSite = webSites[indexPath.row]
            
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}
